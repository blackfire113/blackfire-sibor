try:
	import RPi.GPIO as GPIO
except:
	print("No modulo RPI")

try:
	from gtts import gTTS
except:
	print("No modulo gTTS")
import os


import sys
import requests
from threading import Thread
from socketIO_client import SocketIO
import time
import datetime
try:
	import pymysql.cursors
except:
	print("No modulo pymysql.cursors")
import simplejson as json
import binascii

config_file = json.load(open("config.json"))
#token_file = json.load(open("token.json"))
try:
	server = sys.argv[1]
except:
	server = config_file["server"]

port = config_file["port"]
#device = {'_id': token_file["_id"],"token":token_file["token"]}

try:
    file = open("token.json","r")
    token_file = json.load(file)
except IOError:
    r = request.post(server + "device/438232384f/token",{})
    if(r.status_code < 300):
        file = open("token.json","w")
        token_file = r.json()
        file.write(json.dumps(token_file))
        file.close()
try:
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(17,GPIO.OUT)
	servo = GPIO.PWM(17,200)
	servo.start(7.5)
except:
	print("no modulo gpio")
class sbc(object):
	device = {"_id":token_file["_id"],"token":token_file["token"]}

	config = False
	in_proccess = False
	def config(self, event):
		print("[config] INICIO")
		self.config = False
		self.device = json.dumps(event)
		self.device = json.loads(self.device)
		self.device.pop('$$hashKey',None)
		print(self.device)
		try:
			#GPIO.setmode(GPIO.BCM)
			for item_gpio in self.device['gpio']:
				item_gpio.pop('$$hashKey',None)
				if (item_gpio['type'] == 'input'):
					GPIO.setup(item_gpio['id'],GPIO.IN)
				if (item_gpio['type'] == 'output'):
					GPIO.setup(item_gpio['id'],GPIO.OUT)
					if (GPIO.input(item_gpio['id']) != item_gpio['estatus']):
						GPIO.output(item_gpio['id'],item_gpio['estatus'])
						print("cambio el estatus")
				if(item_gpio['type'] == 'range'):
					print("Servo config 1")
					GPIO.setup(item_gpio['id'],GPIO.OUT)
					#self.servo = GPIO.PWM(item_gpio['id'],50)
					#self.servo.start(7.5)
					print("Servo config 2")
				print("Id")
				print(item_gpio['id'])
					
		except:
			print("Except Fin config")

		self.config = True
		print("[config] FIN")

	def request(self,event):
		print("[request] INICIO")
		conf = json.dumps(event)
		conf = json.loads(conf)
		gpio = conf['gpio']
		if(self.device['_id'] == conf['_id']):
			if(gpio['type'] == 'input'):
				try:
					GPIO.setup(gpio['id'],GPIO.OUT)
					GPIO.output(gpio['id'],gpio['value'])
					gpio['value'] = GPIO.input(gpio['id'])
				except:
					print("Fin try 1")
				if(gpio['value'] == 0):
					gpio['value'] = False
				if(gpio['value'] == 1):
					conf['gpio']['value'] = True
			elif(gpio['type'] == 'output'):
				try:
					GPIO.setup(gpio['id'],GPIO.OUT)
					if(GPIO.input(gpio['id']) != gpio['value']):
						GPIO.output(gpio['id'],gpio['value'])
				except:
					print("Fin try 2")
			elif(gpio['type'] == 'range'):
				try:
					print("Change servo")
					servo.ChangeDutyCycle(gpio['value'] * 30 / 180)
				except:
					servo.stop()
					print("SE MURIO EL SERVO")
					print("SE MURIO EL SERVO")
					print("SE MURIO EL SERVO")
					print("SE MURIO EL SERVO")
					print("SE MURIO EL SERVO")
					print("SE MURIO EL SERVO")
					print("SE MURIO EL SERVO")
					print("SE MURIO EL SERVO")
			print("value")
			print(gpio['value'])

			#codigo agregado ##########
			if('type' in gpio and gpio['type'] == 'timer'):
				gpio['normal_value'] = gpio['status']
			if('type' in gpio and gpio['type'] == 'partial_time_button'):
				#Inicia el proceso
				gpio['in_proccess'] = True
			#El proceso se encuentra ocupado
			print("ocupado peticion")
			while self.in_proccess == True:
				pass
			print("Salir de ocupado")


			print("Iniciar proceso")
			self.in_proccess = True
			for item in self.device["gpio"]:
				if(item["id"] == gpio["id"]):
					index_item = self.device["gpio"].index(item)
					self.device["gpio"][index_item] = gpio
					self.socketio.emit('response',conf)
			self.in_proccess = False
			print("Salir proceso")
			############

		print("[request] FIN")


	def delMote(self,event):
		print('[delMote] INICIO')


		print('[delMote] FIN')


	def setVoice(self,event):
		print('[setVoice] INICIO')
		conf = json.dumps(event)
		conf = json.loads(conf)

		tts = gTTS(text=conf['message'], lang='es')
		tts.save("sound3.mp3")
		os.system("sound3.mp3")

		print('[setVoice] FIN')


	def setMote(self,event):
		print('[setMote] INICIO')
		conf = json.dumps(event)
		conf = json.loads(conf)
		
		try:
			db = pymysql.connect(host='localhost',user='root', password='', db='gateway', charset='utf8', cursorclass=pymysql.cursors.DictCursor)
			with db.cursor() as cursor:
				
				try:
					cursor.execute("DELETE FROM `lora_customer`.`motes` WHERE `eui` = CONV('" + conf['dev_eui'] + "',16,10);")
					cursor.execute("DELETE FROM `lora_application`.`activemotes` WHERE `eui` = CONV('" + conf['dev_eui'] + "',16,10);")
					cursor.execute("DELETE FROM `lora_networkcontroller`.`motes` WHERE `eui` = CONV('" + conf['dev_eui'] + "',16,10);")
					cursor.execute("DELETE FROM `lora_network`.`motes` WHERE `eui` = CONV('" + conf['dev_eui'] + "',16,10);")
					#eui convert hexadecimal a decimal
					#appEui convert hexadecimal a decimal
					#sessionKey string completo
					# NetworkAddress convert Hexadecimal a decimal (Device address)
					cursor.execute("INSERT INTO `lora_customer`.`motes` (`eui`, `appEui`,`lastRxFrame`) VALUES (CONV('" + conf['dev_eui'] + "',16,10),CONV('" + conf['app_eui'] + "',16,10),0)")
					cursor.execute("INSERT INTO `lora_application`.`activemotes` (`eui`, `appEui`,`sessionKey`,`networkAddress`) VALUES (CONV('" + conf['dev_eui'] + "',16,10), CONV('" + conf['app_eui'] + "',16,10),'" + conf['session_key'] + "',CONV('" + conf['network_address'] + "',16,10));")
					cursor.execute("INSERT INTO `lora_network`.`motes` (`eui`,`appeui`,`networkAddress`,`networkSessionKey`,`upMsgSeqNo`,`downMsgSeqNo`) VALUES (CONV('" + conf['dev_eui'] + "',16,10), CONV('" + conf['app_eui'] + "',16,10),CONV('" + conf['network_address'] + "',16,10),'" + conf['session_key'] + "', 0, 0);")
					cursor.execute("INSERT INTO `lora_networkcontroller`.`motes` (`eui`, `appEui`) VALUES (CONV('" + conf['dev_eui'] + "',16,10),CONV('" + conf['app_eui'] + "',16,10));")
					print("Dispositivo guardado")
					db.commit()
				except:
					print("Error al intentar guardar");
					db.rollback()
			db.close()
		except:
			print("No modulo pymysql prueba git")

		print(conf)

		#self.socketio.emit('login',self.device)
		print('[setMote] FIN')

	def on_connect(self):
		print('[connected] INICIO')
		self.socketio.emit('login',self.device)
		print('[connected] FIN')

	def on_disconnect(self):
		print('[disconnecct] INICIO')
		print('[disconnecct] FIN')
	
	def __init__(self):
		self.socketio = SocketIO(server,port,verify=False, cert=False)
		self.socketio.on('getConfig',self.config)
		self.socketio.on('request',self.request)

		#Para guardar en la base de datos
		self.socketio.on('setMote',self.setMote)
		self.socketio.on('delMote',self.delMote)
		#Para guardar en la base de datos
		
		self.socketio.on('setVoice',self.setVoice)
		self.socketio.on('connect',self.on_connect)
		self.socketio.on('reconnect',self.on_connect)
		self.socketio.on('disconnect',self.on_disconnect)
		self.receive_events_thread = Thread(target = self._receive_events_thread)
		self.receive_events_thread.daemon = True
		self.receive_events_thread.start()
		index_send = 0
		datos={}
        #Espera hasta que entre la configuracion del servidor nube
		
		while self.config != True:
			pass


		
		
		try:
			while True:
				while self.in_proccess == True:
					pass
				self.in_proccess = True
				for item_gpio in self.device["gpio"]:
				
					############## 				timer 			###############
					#if('type' in  item_gpio and item_gpio["type"] == "timer" ):
					#	if('dates' in item_gpio):
					#		for date in item_gpio["dates"]:
					#			for day_week in date["days"]:
					#				if( 'id' in day_week and day_week['id'] == datetime.datetime.today().weekday() ):
					#					period = datetime.datetime.today().replace(hour=int(date['time'].split(":")[0]), minute=int(date['time'].split(":")[1]), second=int(date['time'].split(":")[2]))
					#					start = time.mktime(period.timetuple())
					#					finish = start + float(date['duration'])
					#					if(time.time() > start and time.time() < finish):
					#						item_gpio['in_proccess'] = True
					#					else:
					#						item_gpio['in_proccess'] = False
					#Evita que el request no cambie los items del gpio hasta que termine
					#if('in_proccess' in item_gpio and item_gpio['in_proccess'] == True):
					#	if('normal_value' in item_gpio and item_gpio['normal_value'] == item_gpio['value']):
					#		index_item = self.device["gpio"].index(item_gpio)
					#		value = False
					#		if(item_gpio['normal_value'] == False):
					#			value = True
					#		if(self.device['gpio'][index_item]['value'] != value):
					#			try:
					#				GPIO.setup(self.device["gpio"][index_item]["id"], GPIO.OUT)
					#			except:
					#				print("Fin try")
					#			self.device['gpio'][index_item]['value'] = value
					#			try:
					#				GPIO.output(self.device["gpio"][index_item]["id"],self.device["gpio"][index_item]["value"])
					#			except:
					#				print("Fin try")
					#			self.socketio.emit('response',{'_id':self.device['_id'],'gpio':self.device['gpio'][index_item]})
					#if('in_proccess' in item_gpio and item_gpio['in_proccess'] == False):
					#	if('normal_value' in item_gpio and item_gpio['normal_value'] != item_gpio['value']):
					#		item_gpio['value'] = item_gpio['normal_value']
					#		index_item = self.device["gpio"].index(item_gpio)
					#		try:
					#			GPIO.setup(self.device["gpio"][index_item]["id"], GPIO.OUT)
					#			GPIO.output(self.device["gpio"][index_item]["id"],self.device["gpio"][index_item]["value"])
					#		except:
					#			print("fin try")
					#		self.device['gpio'][index_item]['value'] = item_gpio['normal_value']
					#		del item_gpio['normal_value']
					#		self.socketio.emit('response',{'_id':self.device['_id'],'gpio':self.device['gpio'][index_item]})
					########### 				timer 			###############
				

					############ 		partial_time_button 	###############
					#Vefifica que tenga la propiedad type y que sea de tipo 'partial_time_button'
					if('type' in item_gpio and item_gpio['type'] == 'partial_time_button'):
						#Tiene una fecha para finalizar evento ?
						if('in_proccess' in item_gpio and item_gpio['in_proccess'] and 'finish' in item_gpio):
							#Ya termino ?
							if(float(item_gpio['finish']) < time.time()):
								#Regresar las variables a sus estado original
								if(item_gpio['value'] == 0 or item_gpio['value'] == False):
									item_gpio['value'] = True
								elif(item_gpio['value'] == 1 or item_gpio['value'] == True):
									item_gpio['value'] = False
								try:
									GPIO.setup(item_gpio['id'],GPIO.OUT)
									GPIO.output(item_gpio['id'],item_gpio['value'])
								except:
									print("Fin try")
								#Si ya termino entonces eliminar la variable finish y in_proccess
								del item_gpio['in_proccess']
								del item_gpio['finish']
								#Enviar respuesta de finalizacion
								self.socketio.emit('response',{ "_id" : self.device['_id'] , "gpio" : item_gpio })
						#No tiene el attributo finishi
						elif('in_proccess' in item_gpio and item_gpio['in_proccess'] and 'finish' not in item_gpio):
							#asigno el tiempo que tendra que transcurrir
							try:
								GPIO.setup(item_gpio['id'],GPIO.OUT)
								GPIO.output(item_gpio['id'],item_gpio['value'])
							except:
								print("Fin try")
							item_gpio['finish'] = (time.time() + int(item_gpio['seconds']))
					############ 		partial_time_button 	###############
				self.in_proccess = False
				## proceso de guardado en la nube dispositivos
				if(index_send < 1):
					index_send = 6000000
					try:

						db = pymysql.connect(host='localhost',user='root', password='', db='gateway', charset='utf8', cursorclass=pymysql.cursors.DictCursor)
						with db.cursor() as cursor:
							sql = "SELECT `tb_devices_flow`.`id`,`tb_devices`.`id_default`,`tb_devices_flow`.`measure`,`tb_devices_flow`.`time_measure` FROM `tb_devices_flow` INNER JOIN `tb_devices` ON `tb_devices_flow`.`id_device` = `tb_devices`.`id` WHERE `tb_devices_flow`.`cloud` IS FALSE;"
							cursor.execute(sql)
							for row in cursor:
								print(row)
								datos = {'id':row["id"], 'mac_addr':row["id_default"],'consumo':row["measure"],'datetime':row["time_measure"]}
								r = requests.post("http://"+server+"/check_in",datos)
								print("requests")
								if(r.status_code < 300):
									cursor.execute("UPDATE `tb_devices_flow` SET `tb_devices_flow`.`cloud` = TRUE WHERE `tb_devices_flow`.`id` =%d" % (datos['id']) )
									print("commit")
									db.commit()
						db.close()
					except:
						print("No modulo pymysql prueba git")
				index_send -=1
		except KeyboardInterrupt:
			try:
				GPIO.cleanup()
				print("Fin del programa por teclado")
			except:
				print("Fin del programa por teclado")


	def _receive_events_thread(self):
		self.socketio.wait()

def main():
	sbc()

if __name__ == "__main__":
	main()
